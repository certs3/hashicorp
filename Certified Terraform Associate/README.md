# **TERRAFORM ASSOCIATE CERTIFICATION NOTES**

## 1. Terraform CLI

### 1.1 Initializing Working Directories

#### - Command: init

The `terraform init` command is used to initialize a working directory containing Terraform configuration files. This is the first command that should be run after writing a new Terraform configuration or cloning an existing one from version control. It is safe to run this command multiple times.

![terraform-init](images/terraform-init.png)

More info [HERE](https://www.terraform.io/docs/cli/commands/init.html)

##### Example:

> *Whenever you issue ""terraform init" where do all the configurations saved to*
___

### 1.2 Provisioning Infrastructure
___

### 1.3 Authenticating
___

### 1.4 Writing and Modifying Code

#### - Command: console

The `terraform console` command provides an interactive console for evaluating expressions.

#### - Command: validate
___

### 1.5 Inspecting Infrastructure
___

### 1.6 Importing Infrastructure
___

### 1.7 Manipulating state

#### 1.7.1 Inspecting State

#### - Command: state show

The terraform state show command is used to show the attributes of a single resource in the Terraform state.

##### Usage

`terraform state show [options] ADDRESS`

##### Example: Show a Resource
    $ terraform state show 'packet_device.worker'
    # packet_device.worker:
    resource "packet_device" "worker" {
        billing_cycle = "hourly"
        created       = "2015-12-17T00:06:56Z"
        facility      = "ewr1"
        hostname      = "prod-xyz01"
        id            = "6015bg2b-b8c4-4925-aad2-f0671d5d3b13"
        locked        = false
    }

##### Example: Show a Module Resource

    $ terraform state show 'module.foo.packet_device.worker'

##### Example: Show a Resource configured with count

    $ terraform state show 'packet_device.worker[0]'

##### Example: Show a Resource configured with for_each

    $ terraform state show 'packet_device.worker["example"]'

___

#### 1.7.2 Disaster Recovery

#### - Command: force-unlock

Manually unlock the state for the defined configuration.

This will not modify your infrastructure. This command removes the lock on the state for the current configuration. The behavior of this lock is dependent on the backend being used. Local state files cannot be unlocked by another process.

More info [HERE](https://www.terraform.io/docs/cli/commands/force-unlock.html)

##### Usage

`terraform force-unlock [options] LOCK_ID [DIR]`

Options:
- `-force` - Don't ask for input for unlock confirmation.

##### Example:

> *You have accdientally put lock on a configuration file, what command from below you use to remove the lock and make it available*
___

### 1.8 Managing Workspaces
___

### 1.9 Managing Plugins

#### - Command: providers

The `terraform providers` command shows information about the provider requirements of the configuration in the current working directory, as an aid to understanding where each requirement was detected from.

This command also has several subcommands with different purposes, which are listed in the navigation bar.
___

### 1.10 CLI Configuration

#### - Environment Variables

#### TF_VAR_name

Environment variables can be used to set variables. The environment variables must be in the format `TF_VAR_name` and this will be checked last for a value. For example:

    export TF_VAR_region=us-west-1
    export TF_VAR_ami=ami-049d8641
    export TF_VAR_alist='[1,2,3]'
    export TF_VAR_amap='{ foo = "bar", baz = "qux" }'
___

## 2. Terraform Language

### Functions

### Built-in Functions

The Terraform language includes a number of built-in functions that you can call from within expressions to transform and combine values.

#### 2.1. Numeric Functions
___

#### 2.2. String Functions
___

#### 2.3. Collection Functions

#####  - index Function

`index` finds the element index for a given value in a list.

    index(list, value)

The returned index is zero-based. This function produces an error if the given value is not present in the list.

**Examples**

    > index(["a", "b", "c"], "b")
    1

##### - lookup Function

`lookup` retrieves the value of a single element from a map, given its key. If the given key does not exist, the given default value is returned instead.

    lookup(map, key, default)

**Examples**

    > lookup({a="ay", b="bee"}, "a", "what?")
    ay
    > lookup({a="ay", b="bee"}, "c", "what?")
    what?

#### zipmap Function

`zipmap` constructs a map from a list of keys and a corresponding list of values.

    zipmap(keyslist, valueslist)

Both `keyslist` and `valueslist` must be of the same length. `keyslist` must be a list of strings, while `valueslist` can be a list of any type.

Each pair of elements with the same index from the two lists will be used as the key and value of an element in the resulting map. If the same value appears multiple times in `keyslist` then the value with the highest index is used in the resulting map.

**Examples**

    > zipmap(["a", "b"], [1, 2])
    {
    "a" = 1,
    "b" = 2,
    }

___

#### 2.4. Encoding Functions
___

#### 2.5. Filesystem Functions
___

#### 2.6. Date and Time Functions
___

#### 2.7. Hash and Crypto Functions
___

#### 2.8. IP Network Functions
___

#### 2.9. Type Conversion Functions
___

## Configuration Language

#### Protect Sensitive Input Variables

Often you need to configure your infrastructure using sensitive or secret information such as usernames, passwords, API tokens, or Personally Identifiable Information (PII). When you do so, you need to ensure that this data is not accidentally exposed in CLI output, log output, or source control. Terraform provides several features to help avoid accidentally exposing sensitive data.

#### Usage

##### Set values with environment variables
When Terraform runs, it looks in your environment for variables that match the pattern `TF_VAR_<VARIABLE_NAME>`, and assigns those values to the corresponding Terraform variables in your configuration.

    export TF_VAR_db_username=admin TF_VAR_db_password=adifferentpassword

##### Reference sensitive variables

Flag the database connection string output as sensitive, causing Terraform to hide it.

    output "db_connect_string" {
    description = "MySQL database connection string"
    value       = "Server=${aws_db_instance.database.address}; Database=ExampleDB; Uid=${var.db_username}; Pwd=${var.db_password}"
    +  sensitive   = true
    }

Let's apply terraform and check output:

    $ terraform apply
    random_string.lb_id: Refreshing state... [id=2Mw]
    module.vpc.aws_vpc.this[0]: Refreshing state... [id=vpc-05f973211a47fb6f4]

    ## Output truncated...

    Plan: 0 to add, 0 to change, 0 to destroy.

    Changes to Outputs:
    + db_connect_string = (sensitive value)

    Do you want to perform these actions?
    Terraform will perform the actions described above.
    Only 'yes' will be accepted to approve.

    Enter a value: yes

    Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

    Outputs:

    db_connect_string =

##### Sensitive values in state

When you run Terraform commands with a local state file, Terraform stores the state as plain text, including variable values, even if you have flagged them as `sensitive`. Terraform needs to store these values in your state so that it can tell if you have changed them since the last time you applied your configuration.

    $ grep "password" terraform.tfstate
        "value": "Server=terraform-20210113192204255400000004.ct4cer62f3td.us-east-1.rds.amazonaws.com; Database=ExampleDB; Uid=admin; Pwd=adifferentpassword",
                "password": "adifferentpassword",
    ## Output truncated...

## Sensitive Data in State

Terraform state can contain sensitive data, depending on the resources in use and your definition of "sensitive." The state contains resource IDs and all resource attributes. For resources such as databases, this may contain initial passwords.

When using local state, state is stored in plain-text JSON files.

When using remote state, state is only ever held in memory when used by Terraform. It may be encrypted at rest, but this depends on the specific remote state backend.




